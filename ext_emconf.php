<?php

$EM_CONF[$_EXTKEY] = [
    'title' => '[teufels] Form POST Finisher',
    'description' => 'Provides HTTP POST finisher for EXT:form',
    'category' => 'misc',
    'author' => 'teufels GmbH',
    'author_email' => 'digital@teufels.com',
    'state' => 'stable',
    'version' => '1.0.1',
    'constraints' => [
        'depends' =>
            [
                'typo3' => '11.5.0-12.4.99',
                'form' => '11.5.0-12.4.99',
            ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
