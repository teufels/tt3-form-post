<?php

declare(strict_types = 1);

namespace Teufels\Tt3FormPost\Finisher;

/*
 * This file is part of the "tt3_form_post" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Registry;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class PostFinisher extends \TYPO3\CMS\Form\Domain\Finishers\AbstractFinisher
{


    /**
     * @return string|null
     */
    protected function executeInternal()
    {
        //Get Options
        $url = $this->parseOption('url');
        $username = $this->parseOption('username');
        $password = $this->parseOption('password');
        $variables = $this->parseOption('variables');
        $fieldKeyAsInteger = $this->parseOption('fieldKeyAsInteger');
        $requestMethod = $this->parseOption('requestMethod');

        //if username/password is set add to post fields
        if($username) {
            $postfields['username'] = $username;
        }
        if($password) {
            $postfields['password'] = $password;
        }

        //add variables to postfields
        if (!empty($variables)) {
            foreach ($variables as $key => $value) {
                $postfields[$key] = $value;
            }
        }

        //add form values to postfields
        $formValues = $this->finisherContext->getFormValues();

        if($fieldKeyAsInteger) {
            $formValues = $this->convertFieldKeyToInteger($formValues);
        }

        $postfields['fields'] = $formValues;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if($requestMethod == 'POST') {
            curl_setopt($ch, CURLOPT_POST, TRUE);
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));

        curl_setopt($ch, CURLOPT_HEADER, TRUE);

        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $result = curl_exec($ch);

        return null;
    }

    /**
     * @return array
     **/
    private function convertFieldKeyToInteger($formValues):array {
        $convertedFormValues = [];

        foreach ($formValues as $key => $value) {
            $key = (int) preg_replace('/[^0-9]/', '', $key);
            $convertedFormValues[$key] = $value;
        }

        return $convertedFormValues;
    }

}